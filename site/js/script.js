// convert svg images in img tags to inline svg
jQuery('img[src$=".svg"]').each(function () {
    var $img = jQuery(this);
    var imgID = $img.attr('id');
    var imgClass = $img.attr('class');
    var imgURL = $img.attr('src');

    jQuery.get(imgURL, function (data) {
        // Get the SVG tag, ignore the rest
        var $svg = jQuery(data).find('svg');

        // Add replaced image's ID to the new SVG
        if (typeof imgID !== 'undefined') {
            $svg = $svg.attr('id', imgID);
        }
        // Add replaced image's classes to the new SVG
        if (typeof imgClass !== 'undefined') {
            $svg = $svg.attr('class', imgClass + ' replaced-svg');
        }

        // Remove any invalid XML tags as per http://validator.w3.org
        $svg = $svg.removeAttr('xmlns:a');

        // Check if the viewport is set, if the viewport is not set the SVG wont't scale.
        if (!$svg.attr('viewBox') && $svg.attr('height') && $svg.attr('width')) {
            $svg.attr('viewBox', '0 0 ' + $svg.attr('height') + ' ' + $svg.attr('width'))
        }

        // Replace image with new SVG
        $img.replaceWith($svg);

    }, 'xml');

});



/*This section creates speech bubbles that float next to internal link icons. This is included since the content an icon links to is not immediately obvious. Icons used are from the linear icons CDN. */
$('.lnr-home').on({
    mouseenter: function () {
        //stuff to do on mouse enter
        //get bubble icon and add to body of page
        var image = "<svg class='lnr lnr-bubble'><use xlink:href='#lnr-bubble'></use></svg>";

        $('.pad').append(image);
        //add css styling to the bubble and animate
        $('.lnr-bubble').css({
            opacity: 0,
            visibility: "visible"
        }).animate({
            opacity: 1
        }, 'slow');

        $('.lnr-bubble').css('top', '25px');

        //add text inside the bubble
        var text = "<div class='centered-text'>Home</div>";
        $('.pad').append(text);

        $('.centered-text').css({
            opacity: 0,
            visibility: "visible"
        }).animate({
            opacity: 1
        }, 'slow');
        $('.centered-text').css('left', '5px');
        $('.centered-text').css('top', '37px');

    },
    mouseleave: function () {
        //stuff to do on mouse leave
        $('.lnr-bubble').fadeOut();
        $('.centered-text').fadeOut();
        setTimeout(function () {
            $('.lnr-bubble').remove();
            $('.centered-text').remove();
        }, 10);

        //remove the bubble and text from the document


    }
});

//same concept as above, but for the about page link
$('.lnr-user').on({
    mouseenter: function () {
        //stuff to do on mouse enter
        var image = "<svg class='lnr lnr-bubble'><use xlink:href='#lnr-bubble'></use></svg>";
        $('.pad').append(image);
        $('.lnr-bubble').css({
            opacity: 0,
            visibility: "visible"
        }).animate({
            opacity: 1
        }, 'slow');
        $('.lnr-bubble').css('top', '65px');
        
        //add text inside the bubble
        var text = "<div class='centered-text'>About</div>";
        $('.pad').append(text);

        $('.centered-text').css({
            opacity: 0,
            visibility: "visible"
        }).animate({
            opacity: 1
        }, 'slow');
        $('.centered-text').css('left', '4px');
        $('.centered-text').css('top', '78px');
    },
    mouseleave: function () {
        //stuff to do on mouse leave
        $('.lnr-bubble').fadeOut();
        $('.centered-text').fadeOut();
        setTimeout(function () {
            $('.lnr-bubble').remove();
            $('.centered-text').remove();
        }, 10);

    }
});

//same concept as above, but for the portfolio page link
$('.lnr-book').on({
    mouseenter: function () {
        //stuff to do on mouse enter
        var image = "<svg class='lnr lnr-bubble'><use xlink:href='#lnr-bubble'></use></svg>";
        $('.pad').append(image);
        $('.lnr-bubble').css({
            opacity: 0,
            visibility: "visible"
        }).animate({
            opacity: 1
        }, 'slow');
        $('.lnr-bubble').css('top', '110px');
        
        //add text inside the bubble
        var text = "<div class='centered-text'>Work</div>";
        $('.pad').append(text);

        $('.centered-text').css({
            opacity: 0,
            visibility: "visible"
        }).animate({
            opacity: 1
        }, 'slow');
        $('.centered-text').css('left', '6px');
        $('.centered-text').css('top', '123px');
        
    },
    mouseleave: function () {
        //stuff to do on mouse leave
        $('.lnr-bubble').fadeOut();
        $('.centered-text').fadeOut();
        setTimeout(function () {
            $('.lnr-bubble').remove();
            $('.centered-text').remove();
        }, 10);

    }
});

//same concept as above, but for the contact page link
$('.lnr-envelope').on({
    mouseenter: function () {
        //stuff to do on mouse enter
        var image = "<svg class='lnr lnr-bubble'><use xlink:href='#lnr-bubble'></use></svg>";
        $('.pad').append(image);
        $('.lnr-bubble').css({
            opacity: 0,
            visibility: "visible"
        }).animate({
            opacity: 1
        }, 'slow');
        $('.lnr-bubble').css('top', '150px');
        
        //add text inside the bubble
        var text = "<div class='centered-text'>Contact</div>";
        $('.pad').append(text);

        $('.centered-text').css({
            opacity: 0,
            visibility: "visible"
        }).animate({
            opacity: 1
        }, 'slow');
        $('.centered-text').css('left', '6px');
        $('.centered-text').css('top', '165px');
        $('.centered-text').css('font-size', '8px');
    },
    mouseleave: function () {
        //stuff to do on mouse leave
        $('.lnr-bubble').fadeOut();
        $('.centered-text').fadeOut();
        setTimeout(function () {
            $('.lnr-bubble').remove();
            $('.centered-text').remove();
        }, 10);

    }
});
$(document).ready(function() {
$('.main').css('display', 'none');

$('.main').fadeIn(500);



$('a').click(function(event) {

event.preventDefault();

newLocation = this.href;

$('.main').fadeOut(500, newpage);

});



function newpage() {

window.location = newLocation;

}

});
